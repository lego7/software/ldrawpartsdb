import os
import sys
import requests
import gzip
import shutil
import filecmp

def downloadFile(url, filename):
    response = requests.get(url, stream=True)
    with open(filename, "wb") as file:
        shutil.copyfileobj(response.raw, file)

def extractFile(gzFilename, extractedFilename):
    with gzip.open(gzFilename, "rb") as gzFile:
        with open(extractedFilename, "wb") as extractedFile:
            shutil.copyfileobj(gzFile, extractedFile)

def compareFiles(tmp_folder):
    local_folder = "./"
    dcmp = filecmp.dircmp(local_folder, tmp_folder)
    diff_files = dcmp.diff_files
    result = 0

    # Exclude elements.csv from the comparison
    if "elements.csv" in diff_files:
        diff_files.remove("elements.csv")

    if not diff_files:
        print("No updates needed.")
        result = 0
    else:
        print("Updates needed:")
        result = 1
        for file in diff_files:
            print(f"\tDifferences found in file: {file}")
            root_file_path = os.path.join(local_folder, file)
            tmp_file_path = os.path.join(tmp_folder, file)

            local_lines = []
            tmp_lines = []

            with open(root_file_path, 'r') as root_file, open(tmp_file_path, 'r') as tmp_file:
                local_lines = root_file.readlines()
                tmp_lines = tmp_file.readlines()

            for i, (local_line, tmp_line) in enumerate(zip(local_lines, tmp_lines)):
                if local_line != tmp_line:
                    print(f"\t\tDifference in line {i + 1}:")
                    print("\t\t\tLocal file: ", local_line.strip())
                    print("\t\t\tNew file : ", tmp_line.strip())
                    print()

            if len(local_lines) < len(tmp_lines):
                for j in range(len(local_lines), len(tmp_lines)):
                    print(f"\t\tAdditional line in new file at line {j + 1}:")
                    print("\t\t\tNew file: ", tmp_lines[j].strip())
                    print()

            if len(local_lines) > len(tmp_lines):
                for k in range(len(tmp_lines), len(local_lines)):
                    print(f"\t\tLine removed in new file at line {k + 1}:")
                    print("\t\t\tLocal file: ", local_lines[k].strip())
                    print()
        return result;

def copyFiles(origin, destination):
    if not os.path.exists(destination):
        os.makedirs(destination)
    
    for item in os.listdir(origin):
        item_path = os.path.join(origin, item)
        if os.path.isfile(item_path):
            shutil.copy(item_path, destination)
        elif os.path.isdir(item_path):
            shutil.copytree(item_path, os.path.join(destination, item))




def main():
    downloadfrom = "none"
    repo = "repository"
    external = "rebrickable"
    tmpfolder = "./.tmp/"

    if "-r" in sys.argv:
        downloadfrom = repo
    elif "-R" in sys.argv:
        downloadfrom = external
    else:
        print("No source selected. Please choose either '-R' for the repository or '-D' for Rebrickable.")
        exit(-1)

    if downloadfrom == repo:
        urls = [
            "https://gitlab.com/lego7/software/ldrawpartsdb/-/raw/master/elements.csv",
            "https://gitlab.com/lego7/software/ldrawpartsdb/-/raw/master/manualelements.csv",
            "https://gitlab.com/lego7/software/ldrawpartsdb/-/raw/master/renameparts.csv"
        ]
        filenames = [
            "elements.csv",
            "manualelements.csv",
            "renameparts.csv"
        ]
    elif downloadfrom == external:
        urls = [
            "https://cdn.rebrickable.com/media/downloads/elements.csv.gz?1683915759.3426921"
        ]
        filenames = [
            "elements.csv.gz",
        ]
    else:
        print("Invalid source selected. Please choose either '-r' for the repository or '-R' for Rebrickable.")
        exit(-2)

    print("Downloading from " + downloadfrom + "...")
    os.makedirs(tmpfolder, exist_ok=True)
    for url, filename in zip(urls, filenames):
        downloadFile(url, os.path.join(tmpfolder, filename))

    print("Files downloaded successfully from the repository.")

    print("Checking for files differences...")
    if compareFiles(tmpfolder) == 0:
        shutil.rmtree(tmpfolder)
        print("Exiting the program.")
        exit(0)
    
    # Prompt user to continue
    while True:
        user_input = input("Do you wish to continue with the update? (Y/N): ")
        if user_input.lower() == 'y':
            # Continue with the update
            break
        elif user_input.lower() == 'n':
            shutil.rmtree(tmpfolder)
            print("Exiting the program.")
            exit(0)
        else:
            print("Invalid input. Please enter 'Y' to continue or 'N' to exit.")

    print("Updating...")
    copyFiles(tmpfolder, ".")
    shutil.rmtree(tmpfolder)
    print("Files updated successfully.")
    exit(0)

    
    
    
    

if __name__ == "__main__":
    main()
