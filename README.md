# Project PartsDB README
## Welcome to the LEGO Parts Database Project!

This GitLab repository serves as a centralized hub for managing and organizing LEGO parts data. The project aims to provide a comprehensive collection of information about LEGO elements, including their unique identifiers, part numbers, color codes, and mapping between different naming conventions.

## About the Project
The LEGO Parts Database is an essential resource for enthusiasts, collectors, developers, and researchers working with LEGO products. It incorporates data from various sources, including the [Rebrickable website](https://rebrickable.com/home/), which offers a comprehensive database of LEGO parts, sets, colors, and inventories.

The provided files in this repository enable me to programmatically access and manipulate LEGO parts data. This integration with Python scripts allows me to automate processes such as verifying part availability, identifying color inconsistencies, and retrieving the accurate names of the parts.

## In this repository, you will find the following files:

[elements.csv ](./elements.csv ): This file contains a comprehensive list of LEGO elements and their properties, sourced directly from the [Rebrickable website ](https://rebrickable.com/downloads/). It includes element IDs, part numbers, and color IDs, providing a foundational dataset for working with LEGO parts.

[manualelements.csv ](./manualelements.csv ): This file allows for manual additions of parts that may not be present in the elements.csv file. You can add custom elements with their respective element IDs, part numbers, and color IDs to expand the dataset as needed.

[renameparts.csv ](./renameparts.csv ): This file facilitates the translation of part names between different naming conventions. It specifically focuses on translating LDraw part names to Rebrickable part names, enabling seamless integration between the two systems.

update.py: This program automates the process of updating the LEGO Parts Database by downloading the latest files from the repository or Rebrickable. It compares the downloaded files with the existing files, identifies differences, and prompts the user to continue with the update. Upon confirmation, it copies the updated files to the appropriate locations, ensuring that the LEGO Parts Database is up to date.

### How to Use update.py
To update the LEGO Parts Database using the update.py program, follow these steps:

Ensure that you have Python installed on your system.
Clone this GitLab repository to your local machine.
Open a terminal or command prompt and navigate to the repository's directory.

Run the update.py program by executing the following command:

python update.py -R
or
python update.py -r

The -R option specifies that you want to download the files from Rebrickable. 
Use -r if you want to download the files from the repository instead (only elements.csv).

The program will download the necessary files and compare them with the existing files in the repository. It will display any differences found.
You will be prompted to confirm whether you want to continue with the update. Enter Y to proceed or N to exit the program.
If you choose to continue, the program will copy the updated files to their appropriate locations, ensuring that the LEGO Parts Database is up to date.
Once the update is complete, the program will display a message indicating the successful update.
You can now utilize the updated LEGO Parts Database for your LEGO-related endeavors.

## Getting Started
To begin utilizing the LEGO Parts Database Project, you can clone this GitLab repository to your local machine. The files in the root directory contain valuable data and mappings that can be leveraged for various purposes, such as building inventory management systems, cataloging LEGO collections, or conducting research on LEGO elements.

Feel free to explore and utilize the provided files according to your needs. If you encounter any issues or have suggestions for improvements, please don't hesitate to open an issue or submit a pull request.

## Repository Structure
elements.csv: [elements.csv ](./elements.csv )
manualelements.csv: [manualelements.csv ](./manualelements.csv )
renameparts.csv: [renameparts.csv ](./renameparts.csv )
You can access the repository [here](https://gitlab.com/lego7/software/partsdb). We hope this LEGO Parts Database Project proves to be a valuable resource for your LEGO-related endeavors!